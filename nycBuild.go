/**
 ** This is the demo project for showing the API callbacks over HTTP in Go and data etl on MongoDB.
 ** The callbacks are acheived using the POST method and there are 2 callbacks.
 ** The first callback at /latest will take a user input parsed in JSON format with the field
 ** ye as int datatype. This will show the buildings with construct year greater than and equal to
 ** the user input. The second API gives an average of the roof height over all the buildings in
 ** the manhattan area and returns the value. For the database i have selected the fields Bin, ConstrctYr,
 ** HeightRoof and FeatCode as i only needed these for the APIs I worked on.
 **
 ** @author="Vipul Koul"
 */

package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Struct datatype named bfoot for using the data received from csv file or database in JSON format
type bfoot struct {
	Bin        int64   `json:"bin,omitempty" bson:"bin,omitempty" `
	ConstrctYr int     `json:"constrctYr,omitempty" bson:"constrctYr,omitempty" `
	HeightRoof float64 `json:"heightRoof,omitempty" bson:"heightRoof,omitempty" `
	FeatCode   int     `json:"featCode,omitempty" bson:"featCode,omitempty" `
}

//Struct datatype named year for using the data received from the user in JSON format
type year struct {
	Ye int `json:"ye" bson:"ye"`
}

var collection *mgo.Collection // variable for connection to MongoDB

// A function to read the csv file and store it in the MongoDB
func readCSV(path string) {
	f, _ := os.Open(path)
	r := csv.NewReader(bufio.NewReader(f))
	defer f.Close()
	for {
		value, err := r.Read()
		if err == io.EOF {
			break
		}
		if value[1] != "BIN" {
			bin, _ := strconv.ParseInt(value[1], 10, 64)
			constrctYr, _ := strconv.Atoi(value[2])
			heightRoof, _ := strconv.ParseFloat(value[7], 64)
			featCode, _ := strconv.Atoi(value[8])
			row := &bfoot{
				Bin:        bin,
				ConstrctYr: constrctYr,
				HeightRoof: heightRoof,
				FeatCode:   featCode,
			}
			er := collection.Insert(row)

			if er != nil {
				log.Fatal(er)
			}

			fmt.Println("Insertion successful")
		}
	}
	fmt.Println("Database insertion completed")
}

// A function to respond to the POST API call over the /latest url. This returns the buildings after a particular year
func sender(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var data []bfoot
	var y year
	y.Ye = 2000
	_ = json.NewDecoder(request.Body).Decode(&y)
	fmt.Println(&y)
	err := collection.Find(bson.D{{"constrctYr", bson.D{{"$gte", &y.Ye}}}}).All(&data)
	if err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(response).Encode(data)

}

// A function to respond to the GET API call which will return the avg of all the roof height of buildings in Manhattan
func avgHeights(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var data []bfoot
	sum := 0.0
	err := collection.Find(bson.D{{"bin", bson.D{{"$lt", 2000000}}}}).All(&data)
	if err != nil {
		log.Fatal(err)
	}
	for _, value := range data {
		sum += value.HeightRoof
	}
	avg := sum / float64(len(data))
	json.NewEncoder(response).Encode(avg)

}

// The main function where database connection on localhost occurs and also where we call the function to parse the
// csv file along with setting up a HTTP server which is hosted on localhost:7200 and can entertain the above 2 APIs call
func main() {
	session, e := mgo.Dial("mongodb://localhost:27017")
	if e != nil {
		log.Fatal(e)
	}
	fmt.Println("Connected to MongoDB!")
	collection = session.DB("Test").C("NYC")
	fmt.Println("The data from the csv will now be written into MongoDB ")
	path := "./building.csv"
	readCSV(path)
	router := mux.NewRouter()
	router.HandleFunc("/latest", sender).Methods("POST")
	router.HandleFunc("/avg", avgHeights).Methods("GET")
	log.Fatal(http.ListenAndServe(":7200", router))

}
