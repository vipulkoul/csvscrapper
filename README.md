# CSVscrapper

THIS IS THE README FOR THE DEMO !!

Please follow the steps to start the program-
1. Start your MongoDB database on and if its different than localhost:27017 then update it on line 115
2. Check if you have mux and mgo.v2 installed as external packages for program to run
3. Run the go program and it will connect to Database first and then start inserting data into it
4. This will go on for about 4-5 mins approx
5. Then the program will listen for the HTTP API calls on localhost:7200 as its mainpage 
6. Use localhost:7200/latest (POST method) to retreive the dataset for buildings constructed after a user passed number or on value year 2000 by default
7. Use localhost:7200/avg (GET method) to get the avg of all the rooftop heights for the buildings in Manhattan district.


THANKS FOR READING & HAVE A GREAT DAY!!